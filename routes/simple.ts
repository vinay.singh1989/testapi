import { Request, Response } from "express";
import { login } from "../controller/loginController";
import { product } from "../controller/productController";
export class simple{
    private login:login = new login();
    private product:product = new product();
    constructor(){}
    
    public simpleRoutes(app){
        app.route('/').get((req: Request, res: Response) => {
            res.status(200).send({status: 200, message: 'home'});
        });
        app.route('/registration').post((req: Request, res: Response) => {
            console.log('this is vinay');
            /*this.login.userregistration(req, res);*/
        });
        app.route('/login').post((req: Request, res: Response) => {
            this.login.userlogin(req, res);
        });

        app.route('/displaydata').get((req: Request,res: Response) =>{
            this.login.displaydata(req,res);
        });

        app.route('/product-list').get((req: Request,res: Response) =>{
             this.product.productList(req,res);
        });
        app.route('/product-details/:id').get((req: Request,res: Response) =>{
             this.product.productDetails(req,res);
        });
    }
}