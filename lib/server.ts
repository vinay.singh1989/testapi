import app from "../app/app";
const PORT = process.env.PORT || 8200;
app.listen(PORT, () => {
    console.log('listening on port ' + PORT);
})