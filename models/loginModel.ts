import { Request, Response } from "express";
import { connection } from '../config/conn';
import * as jwt from 'jsonwebtoken';

export class loginModel {
    private con: connection = new connection();
    constructor(){}
    
    public async userRegistration(req:Request, res:Response){
        var data = {
            username: req.body.username,
            email: req.body.email,
            password: req.body.password,
            status: 'false'
        }
        console.log(data);
        let query = 'insert into user_info set ?';
        let checkdatas = await this.con.postexecuteQuery(query, data);
        
    }

    public async userLogin(req:Request,res:Response){
        let query = "select * from user_info where email = '"+req.body.email+"'";
        let checkdatas = await this.con.getexecuteQuery(query);
        const testObject = { id: checkdatas[0].id };
        let token = jwt.sign(testObject, process.env.Secret_key,{ expiresIn: 60 * 60 });
        res.send({status: 200, message: 'success', data: checkdatas, token: token});
    }

    public async alluserdata(req:Request, res:Response){
        let query = "select * from user_info";
        let data = await this.con.getexecuteQuery(query);
        console.log(data);
    }
}