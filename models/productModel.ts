import { Request, Response } from "express";
import { connection } from '../config/conn';
import * as jwt from 'jsonwebtoken';

export class productModel {
    private con: connection = new connection();
    constructor(){}
    
    public async allproduct(req:Request, res:Response){
       
       let query = "SELECT * FROM `product_list`";

       let data = await this.con.getexecuteQuery(query);
       res.json({status: 200, message: 'success', data: data});
      
    }

    public async singleproduct(req:Request, res:Response){
      let id = req.params.id;
       let query = "SELECT * FROM `product_list` as prd JOIN product_details AS prod ON prd.id=prod.pr_id WHERE pr_id = "+id;
       let data  = await this.con.getexecuteQuery(query);
       res.json({status: 200, message: 'success', data: data});
      
    }
}