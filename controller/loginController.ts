import { Request, Response } from "express";
import { loginModel } from '../models/loginModel';

export class login{
    private logModel: loginModel = new loginModel();
    constructor(){}
    public async userregistration(req:Request, res:Response){
        this.logModel.userRegistration(req, res);
    }
    public async userlogin(req:Request,res:Response){
        this.logModel.userLogin(req,res);
    }
    public async displaydata(req:Request,res:Response){
    	this.logModel.alluserdata(req,res);
    }


}