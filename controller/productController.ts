import { Request, Response } from "express";
import { productModel } from '../models/productModel';

export class product{

    private productModel: productModel = new productModel();
   
    public async productList(req:Request,res:Response){
    	this.productModel.allproduct(req,res);
    }
    public async productDetails(req:Request,res:Response){
    	this.productModel.singleproduct(req,res);
    }


}