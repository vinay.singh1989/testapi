import * as sql from "mysql";
import { dbhost } from "./config";

export class connection{
    constructor(){
    }

    public conn(){
        var con = sql.createConnection(dbhost);
        con.connect((err) => {
            if(err){
                console.error('error connecting: ' + err.stack);
                return;
            }else{
                console.log('database connected successfully')
            }
        });
        return con;
    }
    
    postexecuteQuery(query, data) {
        let con = this.conn();
        return new Promise<any>(resolve => {
            con.query(query,data, (err, result, fields) => {
                try {
                    if (typeof result == "undefined") {
                        throw "No records in fetchPriority 2"+ query;
                    } else {
                        resolve(result);
                    }
                } catch (err) { this.handleSQLError(err); }
                con.end();
            });
        });
    }

    getexecuteQuery(query) {
        let con = this.conn();
        return new Promise<any>(resolve => {
            con.query(query, (err, result, fields) => {
                try {
                    if (typeof result == "undefined") {
                        throw "No records in fetchPriority 2"+ query;
                    } else {
                        resolve(result);
                    }
                } catch (err) { this.handleSQLError(err); }
                con.end();
            });
        });
    }

    handleSQLError(err) {
        console.error(err);
    }
    
}