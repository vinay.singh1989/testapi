import * as express from 'express';
import * as bodyParser from 'body-parser';
import { simple } from '../routes/simple';
import { secure } from '../routes/secure';
import { Router, Request, Response, NextFunction } from 'express';
import * as jwt from 'jsonwebtoken';

class App {
    public app: express.Application;
    public siRoute: simple = new simple();
    public seRoutes: secure = new secure();
    public router: Router = express.Router();
    constructor(){
        this.app = express();
        this.config();
        this.siRoute.simpleRoutes(this.app);
        this.seRoutes.secureRoutes(this.router);
    }
    private config(): void{
        this.app.use('/secure-api',this.router);
        this.router.use(this.checkToken);
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: false }));
        
        this.app.use(function(req, res, next) {
            res.header('Access-Control-Allow-Origin', '*');
            res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, x-access-token, authorization, token');
            next();
        });
    }

    private checkToken(req: Request, res: Response, next: NextFunction): void {
        let token =  req.headers['token'] || req.headers.token || req.query.token || (req.body == undefined ? req.headers.token : req.body.token);
        if(token){
            jwt.verify(token, process.env.Secret_key, (err:any, data:any) => {
                if(err){
                    res.send({status:403, message: 'Please enter valid token'});
                }else{
                    req.headers.id = data.id;
                    next();
                }
            });            
        }else{ res.send('Please send a token'); }
    }
}

export default new App().app;
